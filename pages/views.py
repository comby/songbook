from django.views.generic import TemplateView
from core.views import LastUpdatedMixin

class HomePageView(LastUpdatedMixin,TemplateView):
    template_name = 'pages/home.html'


class AboutPageView(TemplateView):
    template_name = 'pages/about.html'
