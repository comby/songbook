#!/usr/bin/env python
from django.urls import path, include
from . import views


urlpatterns = (
    # urls for Book
    path('book/', views.BookListView.as_view(), name='book_list'),
    path('book/create/', views.BookCreateView.as_view(), name='book_create'),
    path('book/detail/<int:pk>/', views.BookDetailView.as_view(), name='book_detail'),
    path('book/update/<int:pk>/', views.BookUpdateView.as_view(), name='book_update'),
)

urlpatterns += (
    # urls for Layout
    path('theme/', views.LayoutListView.as_view(), name='layout_list'),
    path('theme/create/', views.LayoutCreateView.as_view(), name='layout_create'),
    path('theme/detail/<slug:slug>/', views.LayoutDetailView.as_view(), name='layout_detail'),
    path('theme/update/<slug:slug>/', views.LayoutUpdateView.as_view(), name='layout_update'),
)

urlpatterns += (
    # urls for song
    path('song/', views.SongListView.as_view(), name='song_list'),
    path('song/create/', views.SongCreateView.as_view(), name='song_create'),
    path('song/detail/<slug:slug>/', views.SongDetailView.as_view(), name='song_detail'),
    path('song/update/<slug:slug>/', views.SongUpdateView.as_view(), name='song_update'),
)
