#!/usr/bin/env python

from django.core.management.base import BaseCommand
from django.conf import settings
from django.utils.text import slugify

from core.models import Artist, Song
from django.db import transaction
import os
import re
import sys

SONGS_DIRS = os.path.join(settings.MEDIA_ROOT,"songs")

REPLACE_DICT = {"\\\\dots":"...",
                "~"," ",
}




reTitle  = re.compile('(?<=beginsong){(.*?)}')
reArtist = re.compile('(?<=by=){(.*?)}')
reAlbum  = re.compile('(?<=album=){(.*?)}')

reCover = re.compile('(?<=cov=){(.*?)}')
@transaction.atomic
def import_song(filepath,song_dir):
    "Import one song in the database from a file"
    with open(os.path.join(song_dir,filepath),'r+') as f:
        data = f.read()
        title = reTitle.search(data).group(1)
        artist_name = reArtist.search(data)
        if artist_name:
            artist_name = artist_name.group(1)
        else:
            artist_name = ''
            print(data)
        match  = reAlbum.search(data)
        if match:
            album = match.group(1)
        else:
            print("empty album")
            album = ''

        match  = reCover.search(data)
        if match:
            cover ="songs/"+os.path.basename(os.path.dirname(filepath))+'/'+match.group(1)+'.jpg'
        else:
            print("no covers")
            cover = 'default/cover-default.jpg'


        artist_slug = slugify(artist_name)
        artist_model, created = Artist.objects.get_or_create(
            slug=artist_slug,
            defaults={'name': artist_name}
        )
        if not created:
            if (artist_model.name != artist_name):
                print("equals slugs db : {}/{}".format(artist_model.name,artist_name))

        title_slug = slugify(title)
        filepath_rel= os.path.relpath(filepath,song_dir)

        song_model, created = Song.objects.get_or_create(
            name = title,
            artist=artist_model,
            defaults= {
                'name': title,
                'lyrics': filepath_rel,
                'album': album,
                'cover': cover
            }
        )
        artist_model.save()
        song_model.save()

# TODO : finir l'import artiste et faire import
#



class Command(BaseCommand):
    args = ""
    help = "reset database to the content in media/songs/"

    def handle(self,*args,**options):
        for root,_dirs,filenames in os.walk(SONGS_DIRS,
                                            topdown=True,
                                            onerror=_file_error,
                                            followlinks=False):
            for filename in filenames:
                if filename.lower().endswith(".sg"):
                    filepath = os.path.realpath(os.path.join(root,filename))
                    try:
                        import_song(filepath,SONGS_DIRS)
                    except:
                        self.stderr.write("*** Failed processing file : "
                                          + filepath)
                        self.stderr.write(sys.exc_info()[0])
def _file_error(error):
    print(error)
