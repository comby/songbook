#!/usr/bin/env python

from django import forms
from .models import Book, Layout, Song


class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = ['name']


class LayoutForm(forms.ModelForm):
    class Meta:
        model = Layout
        fields = ['name', 'file']


class SongForm(forms.ModelForm):
    class Meta:
        model = Song
        fields = ['name','artist', 'album', 'cover', 'lyrics']
