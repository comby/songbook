# Create your views here.
from django.views.generic import DetailView, ListView, UpdateView, CreateView, TemplateView
from django.views.generic.base import ContextMixin
from .models import Book, Layout, Song
from .forms import BookForm, LayoutForm, SongForm
from django.apps import apps


class LastUpdatedMixin(ContextMixin):
    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context["updated_objects"] = {model._meta.verbose_name_plural:
                                      list(model.objects.all().order_by('-last_updated')[:10]) for model in [Book, Layout, Song]}
        return context

# Books
class BookListView(ListView):
    model = Book


class BookCreateView(CreateView):
    model = Book
    form_class = BookForm


class BookDetailView(DetailView):
    model = Book


class BookUpdateView(UpdateView):
    model = Book
    form_class = BookForm

# Layouts
class LayoutListView(ListView):
    model = Layout


class LayoutCreateView(CreateView):
    model = Layout
    form_class = LayoutForm


class LayoutDetailView(DetailView):
    model = Layout


class LayoutUpdateView(UpdateView):
    model = Layout
    form_class = LayoutForm

#Songs
class SongListView(ListView):
    model = Song
    paginate_by = 20

class SongCreateView(CreateView):
    model = Song
    form_class = SongForm


class SongDetailView(LastUpdatedMixin,DetailView):
    model = Song
    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['chunks'] = self.get_object().format_lyrics()
        return context
class SongUpdateView(UpdateView):
    model = Song
    form_class = SongForm
