from django.contrib import admin
from django import forms
from .models import Book, Layout, Song

class BookAdminForm(forms.ModelForm):

    class Meta:
        model = Book
        fields = '__all__'


class BookAdmin(admin.ModelAdmin):
    form = BookAdminForm
    list_display = ['name', 'created', 'last_updated']
    readonly_fields = ['name', 'created', 'last_updated']

admin.site.register(Book, BookAdmin)


class LayoutAdminForm(forms.ModelForm):

    class Meta:
        model = Layout
        fields = '__all__'


class LayoutAdmin(admin.ModelAdmin):
    form = LayoutAdminForm
    list_display = ['name', 'slug', 'created', 'last_updated', 'file']
    readonly_fields = ['name', 'slug', 'created', 'last_updated', 'file']

admin.site.register(Layout, LayoutAdmin)


class SongAdminForm(forms.ModelForm):

    class Meta:
        model = Song
        fields = '__all__'


class SongAdmin(admin.ModelAdmin):
    form = SongAdminForm
    list_display = ['name', 'slug', 'created', 'last_updated', 'artist', 'album', 'cover', 'lyrics']
    readonly_fields = ['name', 'slug', 'created', 'last_updated', 'artist', 'album', 'cover', 'lyrics']

admin.site.register(Song, SongAdmin)
