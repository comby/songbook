from django.urls import reverse
from django_extensions.db.fields import AutoSlugField
from django_extensions.db import fields as extension_fields
from django.db import models as models
from django.db.models import CharField, DateTimeField, FileField, ImageField, TextField, ForeignKey
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth import models as auth_models

from collections import OrderedDict
import os
import re

class Artist(models.Model):
    name = models.CharField(max_length=100, verbose_name='Nom')
    slug = models.SlugField(max_length=100, unique=True)

    class Meta:
        ordering = ["name"]

class Book(models.Model):

    # Fields
    name = CharField(max_length=255)
    created = DateTimeField(auto_now_add=True, editable=False)
    last_updated = DateTimeField(auto_now=True, editable=False)
    is_public = models.BooleanField(default=False)


    class Meta:
        ordering = ('-created',)
        verbose_name = "recueil"
        verbose_name_plural = "recueils"

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('book_detail', args=(self.pk,))


    def get_update_url(self):
        return reverse('book_update', args=(self.pk,))


class Layout(models.Model):

    # Fields
    name = CharField(max_length=255)
    slug = AutoSlugField(populate_from='name', blank=True)
    created = DateTimeField(auto_now_add=True, editable=False)
    last_updated = DateTimeField(auto_now=True, editable=False)
    file = FileField(upload_to="templates/")


    class Meta:
        ordering = ('-created',)
        verbose_name = "theme"
        verbose_name_plural = "themes"
    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('theme_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('theme_update', args=(self.slug,))


REPLACE_DICT=OrderedDict([
   ('\\\\echo{(.*?)}','<i>\g<1></i>'),
    ('\\\\rep{(.*?)}','x<i>\g<1></i>'),
    ('{?\\\\shrp}?',"#"),
    ('\\\\musicnote{(.*?)}','\g<1>'),
    ('{?\\\\nolyrics{?\s?\\\\\[(.*?)\]\s?}?','<span class="chords-only">\g<1></span>'),
    ('\\\\Intro','Intro'),
    ('\\\\Outro','Outro'),
    ('\\\\\[([\w\s/#&]+)\]([^{])','<span class="chords">\g<1></span>\g<2>'),
    ('\\\\\[([\w\s/#&]+)\]{(.*?)}','<span class="chords">\g<1></span>\g<2>'),
    ('{?\\\\og}?','"'),
    ('{?\\\\fg}?','"'),
    ('{?\\\\dots}?','...')
])


class Song(models.Model):
    def cover_upload(s):
        return "songs/{0}/{1}".format(s.artist,s.cover)
    def lyrics_upload(s):
        return "songs/{0}/{1}.sg".format(s.artist.s.slug)

    # Fields
    name = CharField(max_length=255)
    slug = AutoSlugField(populate_from='name', blank=True)
    created = DateTimeField(auto_now_add=True, editable=False)
    last_updated = DateTimeField(auto_now=True, editable=False)
    artist = ForeignKey(Artist,
                        on_delete=models.CASCADE)
    album = CharField(max_length=255,blank=True)
    cover = ImageField(upload_to = cover_upload,blank=True)
    lyrics = FileField(upload_to = lyrics_upload)

    class Meta:
        ordering = ('-created',)
        verbose_name = "chant"
        verbose_name_plural = "chants"

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('song_detail', args=(self.slug,))

    def get_update_url(self):
        return reverse('song_update', args=(self.slug,))

    def format_lyrics(self):
        with open(os.path.join(settings.MEDIA_ROOT,"songs",self.lyrics.name),"r") as f:
            lyrics = f.read()
        # lyrics = re.sub('\\\\\[.*?]','',lyrics) # remove chords
#       lyrics = re.sub('\\\\.*?\\n','',lyrics) # remove latex macros
        chunks = re.findall('\\\\begin{(verse|chorus|verse\*)}(.*?)\\\\end{.*?}',lyrics,re.M|re.S)
        chunks = list(map(list,chunks)) # list of tuple to list of list
        verse_number = 0
        for c in chunks:
            c[1] = re.sub('(?<=[^^])\\n','<br/>\\n',c[1]) #retour a la ligne
            lines = c[1].split("\n")
            for k in range(len(lines)):
                for pattern, new in REPLACE_DICT.items():
                    lines[k] = re.sub(pattern,new,lines[k])
            c[1] = lines
            # add verse number to informations
            if c[0] == "verse":
                verse_number +=1
                c.append(str(verse_number))
            else:
                c.append('')

        return chunks
